#!/usr/bin/env ruby

require_relative './lib/external'
require_relative './lib/diarydb'
require_relative './lib/extract'
require_relative './lib/parser'
require_relative './lib/write'
require_relative './lib/help'
require_relative './lib/list'
require_relative './lib/ui'

require 'table_print'
require 'tty-editor'
require 'fileutils'
require 'humanize'
require 'sqlite3'
require 'json'
require 'pry'

ROOT = __dir__

unless ARGV.empty?
  DiaryDB.initialize unless File.file?("#{ROOT}/database/entries.db")
  $db = Parse::Diary.new
  
  case ARGV[0]
  when 'new' # New entry creation
    Write::User.new
  when 'read' # Searching for and viewing an entry
    case ARGV[1]
    when '-body' # Searching bodies of entries only
      entry = Parse.seek_body(ARGV[2])
      if entry.count > 1
        selected_entry = UI::Entry.selector(entry)
        UI::Entry.display(selected_entry)
      else
        UI::Entry.display(entry)
      end
    when '-index' # Searching for a specific index number
      UI::Entry.display(Parse.seek_index(ARGV[2]))
    else
      entry = Parse.fetch(ARGV[1])
      if entry.count > 1
        selected_entry = UI::Entry.selector(entry)
        UI::Entry.display(selected_entry)
      else
        UI::Entry.display(entry)
      end
    end
  when 'list' # Non-interactive table listing
    case ARGV[1]
    when '-titles'
      List.titles
    when '-dates'
      List.dates
    when '-tags'
      List.tags
    when '-bookmarks'
      List.bookmarks
    else
      List.all
    end
  when 'delete' # Removing entries/tables/db
    case ARGV[1]
    when '-all'
      DiaryDB.delete
    when '-entry' # Removing a single entry
      target = Parse.seek_index(ARGV[2])
      DiaryDB.remove(target.map{ |e| e['index_no'] }[0])
    when '-fromto' # Removing all entries between both index arguments
      x = ARGV[2].to_i
      y = ARGV[3].to_i
      entries = $db.parsed.select{ |e| e['index_no'].between?(x, y) }
      
      entries.flatten.each do |e|
        DiaryDB.remove([e['index_no']][0])
      end
    when '-multi' # Removing multiple entries
      entries = ARGV.drop(2).map{ |e| Parse.seek_index(e) }
      
      entries.flatten.each do |e|
        DiaryDB.remove([e['index_no']][0])
      end
    end
  when 'import'
    External::Import.file(ARGV[1])
  when 'export'
    case ARGV[1]
    when '-json' # Exporting all entries to JSON
      External::Export.to_json($db.parsed)
    else
      entry = Parse.fetch(ARGV[1])
      if entry.count > 1
        selected_entry = UI::Entry.selector(entry)
        External::Export.entry(selected_entry[0])
      else
        External::Export.entry(entry[0])
      end
    end
  when 'count'
    List.count
  when 'help'
    help
  end
else
  help
end

at_exit do
  if ARGV.include?('new') && File.file?("#{ROOT}/tmp.txt")
    File.unlink("#{ROOT}/tmp.txt")
  elsif ARGV.include?('read') && File.file?("#{ROOT}/tmp.txt")
    File.unlink("#{ROOT}/tmp.txt")
  end
end
