A command-line diary program written in Ruby for GNU Linux

![Screenshot](./doc/readingexample.png)

USAGE:
ruby cli-diary[read, new, list, help]

To display all possible commands use "cli-dairy help"

READING an entry can be achieved with "cli-diary read "The Entry I Want To View""
You can search for Titles, Tags, Dates - or words/phrases within the body of your entries.
This will display the contents of the diary entry and provide a prompt to edit, delete, or bookmark.
If multiple entries match your search term you will be prompted to select one.

WRITING your new entry is as simple as "cli-diary new"
This will open your editor where you may write your diary entry. After saving the file -
you will then be asked for a Title and some Tags. Finally, you will then be asked whether to save the entry.

LISTING all your entries "cli-diary list"
This will list all entry Titles, Dates, Tags, and Index numbers in an easily readable table format. 
The command "list" has a few variations - such as "list dates", "list titles", "list tags", and "list bookmarks". 
![Screenshot](./doc/listingexample.png)

IMPORTING a text file as a diary entry is possible with "cli-dairy import "path_to_file""
You will be asked for a Title regardless of the filename. You can Tag this entry just like any other. 

More features available - see 'help'

INSTALLATION:
* Clone this repository to a directory of your choice
* Run "bundle install" in your chosen directory
* DONE!
* Run "ruby cli-diary help" for usage

You can use this program outside of the installed directory by moving it somewhere in your PATH.

TODO:
* Improve documentation.
* Allow for encrypted databases.
* Remember editor choice for quicker entry creation.
* End reliance on external editors(?)
