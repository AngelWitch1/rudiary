module List
  
  def self.all
    puts "Displaying #{$db.count.humanize} entries:"
    puts; tp $db.parsed, :title, :date, :tags, :index_no 
  end

  def self.titles
    $db.parsed.each{ |e| puts e['title'] }
  end

  def self.tags
    tags    = $db.parsed.map{ |e| e['tags'] }.flatten
    counter = Hash.new(0)
    tags.each{ |tag| counter[tag] += 1 }

    counter.each{ |tag, count| puts "#{tag} x#{count}" }
  end
  
  def self.dates
    $db.parsed.each{ |e| puts e['date'] }
  end

  def self.bookmarks
    # TODO: Seperate table for bookmarked entries
    puts; tp $db.parsed.select{ |e| e['bookmarked'] != 0 }, :title, :date, :body, :index_no
  end

  def self.count
    puts $db.count
  end
end
