module Write

  module User

    def self.new
      File.new("#{ROOT}/tmp.txt", 'w+')
      TTY::Editor.open("#{ROOT}/tmp.txt")

      entry  = Array.new
      if ARGV.length > 1 # quick-title provided?
        entry << ARGV[1]
      else
        entry << UI::Prompt.for_s('Title your entry')
      end
      entry << Time.now.asctime
      entry << File.read("#{ROOT}/tmp.txt")
      entry << UI::Prompt.for_s('Tag your entry with one or more tags')
      entry << Parse.indexer
      entry << 0 # Bookmark false by default
      
      if UI::Prompt.for_b('Would you like to save your entry?[y/n]')
        Write::Save.insert(entry)
      else
        exit
      end
    end

    def self.edit(entry)
      File.open("#{ROOT}/tmp.txt", 'w+') do |f|
        f.puts(entry.map{ |e| e['body'] } )
      end
          
      TTY::Editor.open("#{ROOT}/tmp.txt")
      values  = Array.new
      title   = UI::Prompt.for_s('Title your entry')
      if title.size.zero?
        values << Extract.title(entry)
      else
        values << title
      end
      values << Extract.date(entry)
      values << File.read("#{ROOT}/tmp.txt")
      tags   = UI::Prompt.for_s('Tag your entry with one or more tags')
      if tags.size.zero?
        values << Extract.tags(entry)
      else
        values << tags
      end
      values << Extract.index(entry)
      values << Extract.bookmarked?(entry)
          
      # values = entry[0].map{ |k, v| v } all but body
      DiaryDB.remove(values[4])
      Write::Save.insert(values)
    end
  end

  module Save
    
    def self.insert(values)
      # values must be in order: title, date, body, tags, index_no, bookmarked
      $db.entries.execute("INSERT INTO entries VALUES(?, ?, ?, ?, ?, ?)", 
                          [values[0], values[1], values[2], values[3], values[4], values[5]])
      puts 'Saved!'
    end

    def self.indexer
      x = $db.parsed.map{ |e| e['index_no'] }
      x.max + 1
    end

    def self.bookmark(entry)
      values    = entry[0].map{ |k, v| v }
      values[5] = values[5].even? ? 1 : 0
      DiaryDB.remove(values[4])
      self.insert(values)
    end
  end
end
