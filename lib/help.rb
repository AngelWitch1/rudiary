# frozen_string_literal: true

def help
 puts "\
 -------------------
cli-diary.rb [command: new, read, list, etc] [option: -index, -multi, etc] [term: \"an entry title\", \"an entry date\"]

new: 
       Create a new entry - to quick-title your entry, append it inside \"\" 

read:  
       Read the contents of an entry and optionally edit, delete, or bookmark it. 
       To find specific entries, provide a search term inside \"\" - which may be a Title, Date, or Tag. 
       Prepending '-body' before your term will search inside the body of your entries.
       To find an entry by its Index number you can use '-index'.
       While editing an entry you may leave Title/Tags empty to keep originals.

       Examples: read \"A Title\": Searching using an entry Title
                 read \"Jan 1\": Searching using a Date
                 read -body \"Dear Diary..Today I\": Searching inside entries
                 read -index \"5\": Searching for a specific index number

list:  
       Print a list of all entries in table-format.
list tags: 
       Print a list of all Tags from previous entries.
list dates:
       Print a list of Dates from previous entries.
list titles:
       Print a list of Titles from previous entries.
list bookmarks:
       Print a list of all bookmarked entries.

import \"my_file.txt\":
       Create an entry from a txt file.
export:
       Export entries.

       Examples: export \"A Title\": Export an entry to txt file
                 export -json: Export all entries to JSON
 
count:
       Print the number of entries in your diary.
 
delete:
      Delete a single entry, multiple entries, or all entries.
      
      Examples: delete -entry \"1\": Deleting a single entry using its index number
                delete -multi \"1 2\": Deleting multiple entries using their index number
                delete -fromto \"1 6\": Deleting all entries from index 1-6
       
help:
       Print this help page"\
end
