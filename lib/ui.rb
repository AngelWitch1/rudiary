module UI
  module Entry

    def self.display(entry)
      entry.map{ |e| puts "Title: #{e['title']}\nDate: #{e['date']}\nTags: #{e['tags']}\n---\n#{e['body']}" }
      UI::Entry.prompt(entry)
    end
    
    def self.prompt(entry)
      # Prompt given when viewing an entry
      print "---\nedit/delete/bookmark/quit[e/d/b/q]: "
      while (opt = $stdin.gets.chomp)
        case opt
        when 'e', 'E' # Edit
          Write::User.edit(entry)
          break
        when 'd', 'D' # Delete
          if UI::Prompt.for_b('Are you sure you want to delete this entry?[y/n]')
            DiaryDB.remove(entry.map{ |e| e['index_no'] }[0])
            break
          else
            exit
          end
        when 'b', 'B' # Bookmark On/off
          Write::Save.bookmark(entry)
          break
        when 'q', 'Q', 'quit'
          break
        else
          print 'edit/delete/bookmark/quit[e/d/b/q]: '
        end
      end
    end

    def self.selector(entries)
      tp entries, :title, :date, { tags: lambda{ |t| t['tags'] } }, :index_no
      print 'Select an entry index[q to quit]: '
      while (x = $stdin.gets.chomp)
        case x
        when 'q', 'Q'
          exit
        end
        if entries.find{ |e| e['index_no'] == x.to_i }
          selected_entry = entries.select{ |e| e['index_no'] == x.to_i }
          return selected_entry
        else
          print 'Select a valid entry index number: '
        end
      end
    end
  end

  module Prompt
    def self.for_s(prompt)
      # requesting a string
      print "#{prompt}: "
      return $stdin.gets.chomp
    end

    def self.for_b(prompt)
      # requesting Yes or No
      print "#{prompt}: "
      while (yn = $stdin.gets.chomp)
        case yn
        when 'y', 'Y', 'yes'
          return true
        when 'n', 'N', 'no'
          return false
        else
          print "#{prompt}: "
        end
      end
    end
  end
end
