module Extract

  def self.title(entry)
    entry.map{ |e| e['title'] }[0]
  end

  def self.date(entry)
    entry.map{ |e| e['date'] }[0]
  end

  def self.tags(entry)
    entry.map{ |e| e['tags'] }[0]
  end

  def self.index(entry)
    entry.map{ |e| e['index_no'] }[0].to_i
  end

  def self.bookmarked?(entry)
    entry.map{ |e| e['bookmarked'] }[0].to_i
  end
end
