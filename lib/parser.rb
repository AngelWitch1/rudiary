module Parse

  class Diary
    attr_accessor :entries, :parsed

    def initialize
      @entries = SQLite3::Database.open("#{ROOT}/database/entries.db")
      entries.results_as_hash = true
      @parsed  = entries.execute('SELECT * FROM entries')
    end

    def count
      parsed.count
    end
  end

  def self.fetch(query)
    # Case-insensitive wholeword regex
    match = $db.parsed.select do |e|
      e['title'].match(/\b#{query}\b/i) || e['tags'].match(/\b#{query}\b/i) ||
        e['date'].match(/\b#{query}\b/i)
    end

    if match.empty?
      puts "Unable to find a Title, Tag, or Date matching: #{query}"
      if UI::Prompt.for_b('Search inside entries?[y/n]')
        Parse.seek_body(query)
      else
        exit
      end
    else
      match
    end
  end

  def self.seek_body(query)
    # Case-insensitive wholeword regex
    match = $db.parsed.select do |e|
      e['body'].match(/\b#{query}\b/i)
    end

    if match.empty?
      puts "Unable to find an entry matching: #{query}"
      exit
    else
      match
    end
  end

  def self.seek_index(query)
    match = $db.parsed.select{ |e| e['index_no'] == query.to_i }

    if match.empty?
      puts 'No entry matches that Index Number'
      exit
    else
      match
    end
  end

  def self.indexer
    x = $db.parsed.map{ |e| e['index_no'] }
    if x.empty?
      0
    else
      x.max + 1
    end
  end
end
