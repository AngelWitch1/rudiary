module External

  module Import
    
    def self.file(file)
      raise "cannot read #{file}" unless File.file?(file)
      
      values  = Array.new
      values << UI::Prompt.for_s('Title')
      values << Time.now.asctime
      values << File.read(file)
      values << UI::Prompt.for_s('Tag your entry with one or more tags')
      values << Parse.indexer
      values << 0
      
      Write::Save.insert(values)
    end
  end

  module Export

    def self.entry(values)
      if Dir.exist?("#{ROOT}/exports")
        File.open("#{ROOT}/exports/#{values['title']}.txt", 'w+') do |f|
          f.puts(values.values_at('date', 'body')) 
          puts "Exported to #{ROOT}/exports/#{values['title']}.txt"
        end
      else
        FileUtils.mkdir "#{ROOT}/exports"
        File.open("#{ROOT}/exports/#{values['title']}.txt", 'w+') do |f|
          f.puts(values.values_at('date', 'body'))
          puts "Exported to #{ROOT}/exports/#{values['title']}.txt"
        end
      end
    end

    def self.to_json(entries)
      if Dir.exist?("#{ROOT}/exports")
        File.open("#{ROOT}/exports/entries.json", 'w+') do |f|
          f.puts JSON.generate(entries)
        end
      else
        FileUtils.mkdir "#{ROOT}/exports"
        File.open("#{ROOT}/exports/entries.json", 'w+') do |f|
          f.puts JSON.generate(entries)
          puts "Exported to #{ROOT}/exports/"
        end
      end
    end
  end
end
