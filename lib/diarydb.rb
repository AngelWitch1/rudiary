module DiaryDB
  module Template
    
    def self.entry(values)
      # Template for entry creation
      entry = Struct.new(:title, :date, :body, :tags, :index_no, :bookmarked)
      values.each{ |value| entry << value }
      return entry
      # Unused - saving is done by passing values to a table insert method instead
    end
  end

  module Rubbish

    def self.empty
      # Removes entries for good!
    end
  end

  def self.initialize
    FileUtils.mkdir "#{ROOT}/database" unless Dir.exist?("#{ROOT}/database")
    date  = Time.now.asctime
    body  = 'I hope you enjoy using this diary! Refer to the help command for usage. - AngelWitch1@gitlab/miley1@github'
    title = 'Welcome!'
    tag   = 'initial'

    SQLite3::Database.new("#{ROOT}/database/entries.db") do |db|
      db.execute("CREATE TABLE IF NOT EXISTS entries(title TEXT, date TEXT, body TEXT, tags TEXT, index_no INTEGER, bookmarked INTEGER)")
      db.execute("CREATE TABLE IF NOT EXISTS rubbish(title TEXT, date TEXT, body TEXT, tags TEXT, index_no INTEGER, bookmarked INTEGER)")
      db.execute("CREATE TABLE IF NOT EXISTS bookmarks(title TEXT, date TEXT, body TEXT, tags TEXT, index_no INTEGER, bookmarked INTEGER)")
      db.execute("INSERT INTO entries VALUES('#{title}', '#{date}', '#{body}', '#{tag}', 0, 0)")
    end
  end

  def self.table(exdb, title)
    # Creating a new table(diary)
    date = Time.now.asctime
    
    exdb.execute("CREATE TABLE IF NOT EXISTS #{title}(title TEXT, date TEXT, body TEXT, index_no INTEGER, bookmarked INTEGER)")
    exdb.execute("INSERT INTO #{title} VALUES('Welcome!', '#{date}', 'I hope you enjoy using this diary!', 0, 0)") 
  end

  def self.remove(index)
    $db.entries.execute("DELETE FROM entries WHERE index_no = #{index}")
  end
  
  def self.delete
    File.unlink("#{ROOT}/database/entries.db")
  end
end
